## O-RAN - "top-to-bottom" stack change

This session builds upon the prior session's O-RAN/POWDER overview and
simulated demo.  Now that you're familiar with the POWDER O-RAN environment,
you will modify O-RAN components in the RAN and the xApp, changing one of
the closed-loop control algorithms in the NexRAN xApp, using new metrics
from the RAN node.

You will continuing testing your modified code with a single simulated
srsUE, as in the prior O-RAN tutorial session.  Given that you will have
only a single UE, and therefore traffic in a single NexRAN slice, you will
attempt to modify the throttling algorithm to be more precise using the
per-UE and per-slice metrics included in NexRAN's KPM extensions.


### Learning objectives

- Modify RAN and RIC aspects of the NexRAN use case in the POWDER O-RAN profile
- Understand how to redeploy the RIC and xApps
- Learn about details of building software in the RIC ecosystem, including the E2AP and E2SM (service models)

Continues use of the [POWDER O-RAN profile](https://www.powderwireless.net/show/PowderProfiles/O-RAN),
but now modifies the experiment  allocated in the previous
[O-RAN - Near RT RIC](https://gitlab.flux.utah.edu/powderrenewpublic/mww2023/-/blob/main/content/oran.md) session.


### Prerequisites

You will need the one-node O-RAN experiment you created in the [previous
session](https://gitlab.flux.utah.edu/powderrenewpublic/mww2023/-/blob/main/content/oran.md).
If you don't have one, follow the instructions from that session to create
one quickly.  You will want to uncheck the `Install O-RAN SC SMO` profile
parameter when you create your experiment, to speed things up.  You can
install the O-RAN SMO later if necessary.

This session requires a similar terminal configuration oo
This tutorial works best if you have at least four terminals with live SSH
shell connections to your experiment node that you can observe
simultaneously.  If you plan to use the in-browser POWDER shell support, you
will want to open a second copy of the experiment status page in another
browser window---one for the Instructions, and the other for the shells you
will need.


### Source Code Pointers

POWDER O-RAN-enabled srsLTE fork:

  * Source: [https://gitlab.flux.utah.edu/powderrenewpublic/srslte-ric](https://gitlab.flux.utah.edu/powderrenewpublic/srslte-ric)

NexRAN xApp:

  * Source code: [https://gitlab.flux.utah.edu/powderrenewpublic/nexran](https://gitlab.flux.utah.edu/powderrenewpublic/nexran)

  * Dockerfile: [https://gitlab.flux.utah.edu/powderrenewpublic/nexran/-/blob/master/Dockerfile](https://gitlab.flux.utah.edu/powderrenewpublic/nexran/-/blob/master/Dockerfile)

  * Northbound RESTful API: [https://gitlab.flux.utah.edu/powderrenewpublic/nexran/-/blob/master/etc/northbound-openapi.json](https://gitlab.flux.utah.edu/powderrenewpublic/nexran/-/blob/master/etc/northbound-openapi.json)


### Clone the `mww2023` repository onto your O-RAN node

1. This repository contains patch files with the "solutions" to some of the
subsequent steps that instruct you to modify complex C and C++ software.
You can try to do the work manually if you are familiar with srsLTE and
like a challenge, but if we are pressed for time during the live session,
which is likely to be the case, you will probably want to apply the patches
directly.

       cd /local/setup
       git clone https://gitlab.flux.utah.edu/powderrenewpublic/mww2023


### Clean up previous session executions

Make sure you are back at a "clean slate" RIC/RAN deployment:

1. Interrupt the `srsue` process, or `sudo pkill srsue`.

2. Interrupt the `srsenb` process, or `sudo pkill srsenb`.

3. Interrupt the `srsepc` process, or `sudo pkill srsepc`.

4. Unconfigure the existing NexRAN xApp instance:

       /local/repository/demo/cleanup-nexran-simulated.sh

   (NB: this removes all Ue, Slice, and NodeB objects from the xApp, *and*
   from `srsenb` if it is still running.  Removing NodeB objects has the
   important side effect of unsubscribing the xApp from receiving KPM
   indications through the RIC.  There is a bug or feature with the RIC
   `submgr` subscription manager component that does not remove nor
   re-create E2 Subscriptions when RAN nodes disconnect and reconnect.
   Thus, if you restart your `srsenb` process without removing the xApp's
   KPM subscription to your current or prior `srsenb`, the `submgr` will
   believe that new E2 SubscriptionRequests from xApps to this RAN node are
   already in place---and the SubscriptionRequest will *not* be passed along
   to the RAN node, even though the xApp will receive a SubscriptionResponse
   from the RIC.  Always performing this cleanup step prior to restarting
   your srsenb process will keep the RIC's state clean.  If you forget to do
   this in between `srsenb` restarts, you will need to restart the RIC.)

5. Set local environment variables for various RIC service endpoints:

       . /local/repository/demo/get-env.sh

5. Remove the existing NexRAN xApp:

       curl -L -X DELETE http://${APPMGR_HTTP}:8080/ric/v1/xapps/nexran

6. Restart core RIC components:

       kubectl -n ricplt rollout restart \
           deployments/deployment-ricplt-e2term-alpha \
           deployments/deployment-ricplt-e2mgr \
           deployments/deployment-ricplt-submgr \
           deployments/deployment-ricplt-rtmgr \
           deployments/deployment-ricplt-appmgr \
           statefulsets/statefulset-ricplt-dbaas-server

7. Wait until the RIC services are back up:

       kubectl -n ricplt wait pod --for=condition=Ready --all --timeout=3m


### Modify the POWDER O-RAN-enabled srsLTE fork

1. You should already have the POWDER srsLTE fork git repository cloned on
your O-RAN node in `/local/setup/srslte-ric`, and it has been compiled in
`/local/setup/srslte-ric/build`.

2. Using your favorite text editor (e.g. `emacs`, `nano`, `pico`, `vi`,
`vim` are all installed---or skip to the end of this paragraph and apply the
patch), modify `/local/setup/srslte-ric/srsenb/hdr/ric/e2sm_kpm.h` and
`/local/setup/srslte-ric/srsenb/src/ric/e2sm_kpm.cc` to additionally collect
and return the `dl_mcs` and `dl_samples` values.  Our extended E2SM-KPM
service model already has fields for these values, and the profile
downloaded and built the `C`/`C++` bindings that were generated via `asn1c`.
Notice that we are already collecting `ul_mcs` and `ul_samples` in
similarly-named member variables into the `ric::kpm::entity_metrics_t`
structure declared in `/local/setup/srslte-ric/srsenb/hdr/ric/e2sm_kpm.h`;
and we are collecting, indexing, and encoding those values into E2
Indications in `/local/setup/srslte-ric/srsenb/src/ric/e2sm_kpm.cc`.  If you
search these files for `ul_mcs` and `ul_nsamples`, and add nearly identical
lines, but with `dl_` prefixes instead, you'll be done.

   **NB: A patch that provides this change** is also in this repository at
   [content/solutions/patches/srslte-ric-kpm-dl-mcs.patch](https://gitlab.flux.utah.edu/powderrenewpublic/mww2023/-/blob/main/content/solutions/patches/srslte-ric-kpm-dl-mcs.patch).
   You can directly apply it instead of editing these files:

       cd /local/setup/srslte-ric
       patch -p1 < /local/setup/mww2023/content/solutions/patches/srslte-ric-kpm-dl-mcs.patch

3. Rebuild srslte:

       cd /local/setup/srslte-ric/build
       make -j`nproc`


### Modify the POWDER NexRAN xApp

1. Instead of building the NexRAN xApp from source, the profile installed a
pre-built version of the NexRAN Docker image from our gitlab server, which
you can see via:

       docker image ls gitlab.flux.utah.edu:4567/powder-profiles/oran/nexran

   We will next build a custom version of this image.

2. Clone the NexRAN source code repository:

       cd /local/setup
       git clone https://gitlab.flux.utah.edu/powderrenewpublic/nexran

3. Modify NexRAN to collect and dump the `dl_mcs` and `dl_samples` fields.
You will want to edit `/local/setup/nexran/lib/e2sm/include/e2sm_kpm.h` (to
add the `dl_mcs` and `dl_samples`, just after the existing `ul_mcs` and
`ul_samples` fields, with the same types); and
`/local/setup/nexran/lib/e2sm/src/e2sm_kpm.cc` (to retrieve the
corresponding values from the ASN1 message fields; again, look for `ul_mcs`
and `ul_samples` and do likewise).

   **NB: The necessary patch** is in [content/solutions/patches/nexran-kpm-dl-mcs.patch](https://gitlab.flux.utah.edu/powderrenewpublic/mww2023/-/blob/main/content/solutions/patches/nexran-kpm-dl-mcs.patch).
   You can directly apply it instead of editing these files:

       cd /local/setup/nexran
       patch -p1 < /local/setup/mww2023/content/solutions/patches/nexran-kpm-dl-mcs.patch

4. Rebuild the NexRAN docker image.  (NB: the first build will take some
time as many dependent packages are installed.  Future builds will be much
quicker since the `Dockerfile` installs its dependencies in the first
several image layers, and only after those copies in the source code and
builds it.)

       cd /local/setup/nexran \
           && docker build . -t node-0.cluster.local:5000/nexran:test \
           && docker push node-0.cluster.local:5000/nexran:test

   (NB: the profile create a local, private Docker image registry running on
   port 5000; we will next reconfigure the RIC to use this image.)

5. Remove the old xApp descriptor from the RIC `appmgr` service.  (NB: you
can verify which version of the NexRAN xApp descriptor is currently in the
`onboarder` service via the preceding `GET` command, but it should be `0.1.0`.)

       . /local/repository/demo/get-env.sh
       curl -L -X GET "http://${ONBOARDER_HTTP}:8080/api/charts" ; echo
       curl -L -X DELETE "http://${ONBOARDER_HTTP}:8080/api/charts/nexran/0.1.0"

   (NB: in newer RIC versions, e.g. the `f-release`, the onboarder is no
   longer packaged as a service.  Had you selected the `f-release` when you
   created your O-RAN release, you would instead use the `dms_cli` tool via
   the profile's instructions.)

6. Modify the xApp's config descriptor with your favorite editor.  Open the
`/local/profile-public/nexran-config-file.json` file.  Change the
`"version"` value (line 4) from `"0.1.0"` to `"0.1.1"`.  Next, change
the `"registry"` value (line 9) from `"gitlab.flux.utah.edu:4567"` to
`"node-0.cluster.local:5000"`.  Next, change the `"name"` value (line 10)
from `"powder-profiles/oran/nexran"` to `"nexran"`.  Finally, change the
`"tag"` value (line 11) from `"latest"` to `"test"`.  Save the file.

7. Re-onboard the xApp:

       . /local/repository/demo/get-env.sh
       curl -L -X POST \
           "http://${KONG_PROXY}:32080/onboard/api/v1/onboard/download" \
           --header 'Content-Type: application/json' \
           --data-binary "@/local/profile-public/nexran-onboard.url"

8. Make sure that the onboard succeeded (e.g., that the service created a
helm chart with the `0.1.1` version):

       . /local/repository/demo/get-env.sh
       curl -L -X GET "http://${ONBOARDER_HTTP}:8080/api/charts" ; echo


### Redeploy srsLTE and the xApp

1. Instantiate the modified xApp:

       . /local/repository/demo/get-env.sh
       curl -L -X POST \
           "http://${KONG_PROXY}:32080/appmgr/ric/v1/xapps" \
           --header 'Content-Type: application/json' \
           --data-raw '{"xappName": "nexran"}'

2. Look at the xApp's logs:

       kubectl logs -n ricxapp -l app=ricxapp-nexran

   (You can later add `-f` to this command to stream the logs continuously.)

3. Configure the xApp to stream KPM metrics to the RIC's influxdb:

       . /local/repository/demo/get-env.sh
       curl -L -X PUT \
           http://$NEXRAN_XAPP:8000/v1/appconfig \
           -H "Content-type: application/json" \
           -d '{"kpm_interval_index":18,"influxdb_url":"http://'$INFLUXDB_IP':8086?db=nexran"}'

3. Start `srsepc`:

       sudo /local/setup/srslte-ric/build/srsepc/src/srsepc \
           --spgw.sgi_if_addr=192.168.0.1 2>&1 >> /local/logs/srsepc.log &

4. Start `srsenb`:

       . /local/repository/demo/get-env.sh
       sudo /local/setup/srslte-ric/build/srsenb/src/srsenb \
           --enb.n_prb=15 --enb.name=enb1 --enb.enb_id=0x19B \
           --rf.device_name=zmq --rf.device_args="fail_on_disconnect=true,id=enb,base_srate=23.04e6,tx_port=tcp://*:2300,rx_port=tcp://localhost:2301" \
           --ric.agent.remote_ipv4_addr=${E2TERM_SCTP} \
           --ric.agent.local_ipv4_addr=10.10.1.1 --ric.agent.local_port=52525 \
           --log.all_level=warn --ric.agent.log_level=debug --log.filename=stdout \
           --slicer.enable=1 --slicer.workshare=0

   If you are limited on terminals and want to put this command into the
   background, you can append `2>&1 >> /local/logs/srslte.log &` to it.
   (NB: you probably want to `tail -F /local/logs/srslte.log` to watch the
   logfile, because it is interesting to see what the NodeB is doing, but
   you can skip this if you are limited on total terminals or are running in
   the web UI.)

5. Tell the xApp to subscribe to the NodeB, configure slicing policies, and
bind UEs to slices:

       /local/repository/demo/run-nexran-simulated-throttle.sh

6. Check the xApp's logfile and make sure you see incoming KpmReport
messages:

       kubectl logs -n ricxapp -l app=ricxapp-nexran | grep KpmReport

   If you don't see at least one message, something has gone wrong with your
   NodeB, your RIC, or your xApp.  Refer to the `Help!` section and
   troubleshoot.

7. Start a background `ping` process before starting the UE.  (This pings
the UE continually from the EPC's perspective and ensures that the RRC layer
doesn't move into the IDLE state.)

       ping 192.168.0.2 2>&1 >> /local/logs/ping.log &

8. Start `srsue`:

       sudo /local/setup/srslte-ric/build/srsue/src/srsue \
           --rf.device_name=zmq --rf.device_args="tx_port=tcp://*:2301,rx_port=tcp://localhost:2300,id=ue,base_srate=23.04e6" \
           --usim.algo=xor --usim.imsi=001010123456789 \
           --usim.k=00112233445566778899aabbccddeeff --usim.imei=353490069873310 \
           --log.all_level=warn --log.filename=stdout --gw.netns=ue1

   If you are short on terminals, you might want to put `srsue` into the
   background.  If so, append `2>&1 >> /local/logs/srsue.log &` to the above
   command.

9. Check your `ping` process's logs to make sure you are seeing traffic:

       tail /local/logs/ping.log

10. Check your xApp's logs to make sure you are getting E2 KPM Indications
*that now contain metrics*:

        kubectl logs -n ricxapp -l app=ricxapp-nexran | grep KpmReport | tail

11. Start an `iperf` server in the background:

        sudo ip netns exec ue1 iperf -s -p 5010 -i 4 -t 36000 \
            2>&1 >> /local/logs/iperf-server.log &

12. Start an `iperf` client:

        iperf -c 192.168.0.2 -p 5010 -i 4 -t 36000

13. Watch the Grafana dashboard and the iperf client, and observe the
closed-loop throttling policy closed-loop throttling policy adjust slice
resources, and impact the iperf client's reported bandwidth.


### Improving the closed-loop throttling policy

You will have noticed that the simple closed-loop throttling policy
iteratively closed in on the target throttle bandwidth.  If you want to
experiment with the closed-loop policies more, e.g. to see if you could
improve its precision by using RAN metrics like `dl_cqi`, `dl_mcs`, or
others, you would only need to rebuild and redeploy the nexran xApp.  At
minimum, you would want to run

    /local/repository/demo/cleanup-nexran-simulated.sh
    . /local/repository/demo/get-env.sh
    curl -L -X DELETE http://${APPMGR_HTTP}:8080/ric/v1/xapps/nexran
    # modify code in /local/setup/nexran
    cd /local/setup/nexran \
        && docker build . -t node-0.cluster.local:5000/nexran:test \
        && docker push node-0.cluster.local:5000/nexran:test
    curl -L -X POST \
        "http://${KONG_PROXY}:32080/appmgr/ric/v1/xapps" \
        --header 'Content-Type: application/json' \
        --data-raw '{"xappName": "nexran"}'
    /local/repository/demo/run-nexran-simulated-throttle.sh

but you can also re-run the cleanup instructions listed at the beginning of
this tutorial, and redeploy as above.  You might also want to simulate
harsher channel conditions on your simulated link, perhaps by adding some
downlink simulation parameters to your `srsenb` command:

    sudo /local/setup/srslte-ric/build/srsenb/src/srsenb \
        --enb.n_prb=15 --enb.name=enb1 --enb.enb_id=0x19B \
        --rf.device_name=zmq --rf.device_args="fail_on_disconnect=true,id=enb,base_srate=23.04e6,tx_port=tcp://*:2300,rx_port=tcp://localhost:2301" \
        --ric.agent.remote_ipv4_addr=${E2TERM_SCTP} \
        --ric.agent.local_ipv4_addr=10.10.1.1 --ric.agent.local_port=52525 \
        --log.all_level=warn --ric.agent.log_level=debug --log.filename=stdout \
        --slicer.enable=1 --slicer.workshare=0 \
        --channel.dl.enable=1 \
        --channel.dl.awgn.enable=1 --channel.dl.awgn.snr=30 \
        --channel.dl.fading.enable=1 --channel.dl.fading.model=etu300 \
        --channel.dl.hst.enable=1

You might want to re-run your `srsenb` and `srsue` with the harsher channel
conditions and observe their effect in Grafana or on the NexRAN xApp's
kubernetes log stream **before** modifying the code, to further develop your
intuition of profitable changes to make.

Finally, once you start adding noise, delay, and loss to your simulated RAN
link, you will want to switch your iperf server and client to UDP mode
instead of TCP, so that your streams continue to send at a constant rate
even if the channel quality decreases suddenly or if the `srsue` disconnects
briefly.  The new commands will be

    sudo ip netns exec ue1 iperf -u -s -p 5010 -i 4 -t 36000 \
        2>&1 >> /local/logs/iperf-server.log &

and

    iperf -u -c 192.168.0.2 -p 5010 -i 4 -t 36000


### Help!

This section provides suggestions for problems you might encounter.


#### `srsenb` refuses to connect to the RIC

1. Reset the current `e2term` service endpoint that the example `srsenb`
command lines rely upon:

       /local/repository/demo/get-env.sh

   (NB: this should only change if you fully undeploy and redeploy the RIC.
   If you use the less-invasive Kubernetes `rollout restart` strategy, the
   `E2TERM_SCTP` IP address value should not change.)

2. Try to ping the e2term service:

       ping -c 1 $E2TERM_SCTP

3. Restart the core RIC services:

       kubectl -n ricplt rollout restart \
           deployments/deployment-ricplt-e2term-alpha \
           deployments/deployment-ricplt-e2mgr \
           deployments/deployment-ricplt-submgr \
           deployments/deployment-ricplt-rtmgr \
           deployments/deployment-ricplt-appmgr \
           statefulsets/statefulset-ricplt-dbaas-server

4. Wait until the RIC services are back up:

       kubectl -n ricplt wait pod --for=condition=Ready --all --timeout=2m


#### I don't see any KPM Indication messages in the `nexran` logfile's output

This probably means the subscription request did not reach the NodeB.  The
most likely explanation for this is if you restart your `srsenb` without
first telling NexRAN to "delete" the NodeB (which would have triggered a
subscription delete).  You will need to fix your subscription manager
component.  You can try to restart it in isolation, e.g.

    kubectl -n ricplt rollout restart \
           deployments/deployment-ricplt-submgr

but the other services may sometimes not "reconnect" with the restart
subscription manager.  In that case, you will have to fall back to the
broader `rollout restart` above.

#### `curl` commands sent to NexRAN are failing

Make sure you have run

    . /local/repository/demo/get-env.sh

in the terminal in which you are running `curl`.  If that script does not
set the `NEXRAN_XAPP` environment variable to a value, most likely you did
not actually deploy the xApp.
