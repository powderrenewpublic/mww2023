## OpenAirInterface 5G - RAN & Core

### Learning objectives

- Understand how to obtain, configure, and deploy the OAI 5G core network (CN5G) using Docker
- Understand how to obtain, build, and execute OAI 5G RAN components (gNodeB and nrUE)
- Understand how to make use of OAI simulated RF capabilities
- Understand how to make real-time changes to simulated channel models
- Understand how to go from simulated RF capabilities to the various POWDER "real RF" functionality

### Prerequisites

- Laptop with [Chrome](https://www.google.com/chrome/) browser (most tested browser for web-based VNC sessions) 
- POWDER account and membership in `mww2023` Project

### Profile, hardware, and software overview

Profile: https://www.powderwireless.net/show/mww2023/5g-sim

This profile deploys a single server-class compute node (Dell R430) with a disk image that includes:

- OAI CN5G 
  - Docker + docker-compose
  - The OAI CN5G federated [repo](https://gitlab.eurecom.fr/oai/cn5g/oai-cn5g-fed/-/tree/v1.4.0)
    - CN5G NF sources included as git submodules
    - docker-compose files for configuration/deployment
    - helper script for deploying various CN5G configurations
  - Docker images for recent builds of all CN5G NFs
- OAI 5G RAN
  ([features](https://gitlab.eurecom.fr/oai/openairinterface5g/blob/develop/doc/FEATURE_SET.md#openairinterface-5g-nr-feature-set))
  - source code and prebuilt binaries for:
    - gNodeB softmodem 
    - nrUE softmodem
    - nrscope for viewing UL/DL plots
    - RF simulator w/ telnet server for realtime updating of channel model
- VNC server for access via web-based VNC client

### Hands-on

#### Start your experiment (to be done at start of the Mobile Networking session)

1. Log into the POWDER portal at https://www.powderwireless.net
1. Select the [5g-sim](https://www.powderwireless.net/show/mww2023/5g-sim) profile (click this link)
1. Click "Instantiate"
1. If you see a "Project" selection box on the "Finalize" page, select the `mww2023` project and click "Next". Else, just click "Next". **Don't enter an experiment name, the portal will create a random one for you that will avoid collision with other experiment names**.
1. Click "Finish" on the "Schedule" page. **Don't enter a start or end date/time.**

After a bit you should see something like this:

<img src=https://gitlab.flux.utah.edu/uploads/-/system/user/970/d4d916de102b567fb8ee98f321f97d5b/experiment-loading.jpg width=500px>

(The Mobile Networking session will continue while your experiment gets created. We'll come back to it later.)


#### Open an in-browser VNC session

At this point your experiment should be **Ready** and you should see something like this at the top of the experiment page in the portal:

<img src=https://gitlab.flux.utah.edu/uploads/-/system/user/970/eb44f0c52122f6d51a48e3143f65c61b/experiment-ready.jpg width=500px>

Now open a VNC session on your node by clicking on the node in the "Topology View" and selecting "Open VNC window". (Alternatively, you can click on the gear icon to the right of the node in the "List View" and find the same "Open VNC window" option).

<img src=https://gitlab.flux.utah.edu/uploads/-/system/personal_snippet/21/79596bb0c0fb930259b34294137524f9/open-vnc.jpg width=500px>

NB: 

- You may have to allow a pop-up window in your browser to see the window. Also, if see the window but the page is blank, just close that window and try again. 

Next, let's make our window manager a little easier to deal with: 

1. Left-click on an empty space on the virtual desktop and select "FvwmConsole"
1. Type `Style "*" Icon` into the window that pops up and press `enter`

Now, when you minimize windows they will appear as icons on the desktop that you can double-click to maximize again.

#### Examine CN5G images/configuration

NB:

- Copy/paste can be a little odd with these sessions. You may have to first past into the white box at the top of the window and then paste into your shells if your regular shortcuts don't work.
- There is also a cheatsheet on the node that you can view in a shell on the virtual desktop with `less /local/repository/cheatsheet.txt` can copy/paste from if you are having trouble.

Rather than have you all clone the OAI CN5G federated repo and pull down 2 GB worth of images at the same time, we've included these in the disk image on your node. Let's look at the federated repository. Open a terminal on your VNC desktop if there isn't already one there (click on an empty spot on the desktop and select "XTerm") and type the following to list the contents of the `oai-cn5g-fed` repo directory on our node:

    ls /opt/oai-cn5g-fed

You'll see the following:

    dmaas@node:~$ ls /opt/oai-cn5g-fed
    CHANGELOG.md  ci-scripts  CONTRIBUTING.md  docs     openshift  scripts
    charts        component   docker-compose   LICENSE  README.md

The `component` sub-directory contains the source code for all of the 5G core network functions. Let's take a look at those by typing

    ls -l /opt/oai-cn5g-fed/component
    
You'll see the following:

    dmaas@node:~$ ls -l /opt/oai-cn5g-fed/component
    total 36
    drwxr-xr-x  9 dmaas mww2023 4096 Jan 16 22:59 oai-amf
    drwxr-xr-x 10 dmaas mww2023 4096 Jan 16 22:59 oai-ausf
    drwxr-xr-x 11 dmaas mww2023 4096 Jan 16 22:59 oai-nrf
    drwxr-xr-x  9 dmaas mww2023 4096 Jan 16 22:59 oai-nssf
    drwxr-xr-x  9 dmaas mww2023 4096 Jan 16 22:59 oai-smf
    drwxr-xr-x  9 dmaas mww2023 4096 Jan 16 23:00 oai-udm
    drwxr-xr-x  9 dmaas mww2023 4096 Jan 16 23:00 oai-udr
    drwxr-xr-x 10 dmaas mww2023 4096 Jan 16 23:00 oai-upf-equivalent
    drwxr-xr-x  9 dmaas mww2023 4096 Jan 16 23:00 oai-upf-vpp

For convenience, here are is a list of the NF names:

| Abbr | Network Function                          |
|------|-------------------------------------------|
| AMF  | Access and Mobility Management Function   |
| AUSF | Authentication Server Management Function |
| NRF  | Network Repository Function               |
| NSSF | Network Slicing Selection Function        |
| SMF  | Session Management Function               |
| UDM  | Unified Data Management                   |
| UDR  | Unified Data Repository                   |
| UPF  | User Plane Function                       |

We're not going to build these from source today. (If you're interested in doing that at some point, you'll find a a good tutorial [here](https://gitlab.eurecom.fr/oai/cn5g/oai-cn5g-fed/-/blob/v1.4.0/docs/BUILD_IMAGES.md).) Instead, we'll make use of prebuilt images that have been pulled from Docker Hub as described [here](https://gitlab.eurecom.fr/oai/cn5g/oai-cn5g-fed/-/blob/v1.4.0/docs/RETRIEVE_OFFICIAL_IMAGES.md). We can look at the images on our node by typing

    sudo docker images oai\*

You should see docker images that match the contents of the `component` directory:

    dmaas@node:~$ sudo docker images oai\*
    REPOSITORY       TAG       IMAGE ID       CREATED        SIZE
    oai-upf-vpp      v1.4.0    e4b272a861f9   6 months ago   303MB
    oai-spgwu-tiny   v1.4.0    b1d0c95000bc   6 months ago   127MB
    oai-smf          v1.4.0    639a618a3707   6 months ago   149MB
    oai-amf          v1.4.0    890b5b8e62ec   6 months ago   153MB
    oai-udr          v1.4.0    fcfc4be7b807   7 months ago   148MB
    oai-nssf         v1.4.0    84b0e0494976   7 months ago   96MB
    oai-udm          v1.4.0    4eec9f6b1651   8 months ago   143MB
    oai-nrf          v1.4.0    c22af18f9da0   9 months ago   102MB
    oai-ausf         v1.4.0    bd704e547e4e   9 months ago   140MB

We're going to deploy what OAI refers to as the "basic" core network. It includes all of these NFs connected by a docker network. We'll use a helper script provided in the federated repository to do that, but we'll ignore that script today and focus on the `docker-compose-basic-nrf.yaml` file that this script will end up using because of the arguments we provide it. You can look at this file by typing

    less /opt/oai-cn5g-fed/docker-compose/docker-compose-basic-nrf.yaml

or just follow along with me. We'll see that this file refers to the docker images listed above and allows us to further configure the various network functions. We'll just use the defaults today. Later, our OAI gNodeB will connect to the AMF network function via the docker network (NG interface). (You can enter `q` to exit `less`.) Let's go ahead and actually deploy the "basic" 5G core network...

#### Deploy the OAI 5G "basic" core network

You can deploy the 5G core network by typing

    cd /opt/oai-cn5g-fed/docker-compose
    sudo python3 ./core-network.py --type start-basic --scenario 1

You should see output similar to this

    dmaas@node:~$ cd /opt/oai-cn5g-fed/docker-compose/
    dmaas@node:/opt/oai-cn5g-fed/docker-compose$ sudo python3 ./core-network.py --type start-basic --scenario 1
    [2023-01-22 15:54:50,425] root:DEBUG:  Starting 5gcn components... Please wait....
    [2023-01-22 15:54:50,425] root:DEBUG: docker-compose -f docker-compose-basic-nrf.yaml up -d
    ...
    [2023-01-22 15:55:02,947] root:DEBUG:  OAI 5G Core network started, checking the health status of the containers... takes few secs....
    [2023-01-22 15:55:02,947] root:DEBUG: docker-compose -f docker-compose-basic-nrf.yaml ps -a
    [2023-01-22 15:55:19,793] root:DEBUG:  All components are healthy, please see below for more details....
    Name                 Command                  State                  Ports            
    -----------------------------------------------------------------------------------------
    mysql        docker-entrypoint.sh mysqld      Up (healthy)   3306/tcp, 33060/tcp         
    oai-amf      /bin/bash /openair-amf/bin ...   Up (healthy)   38412/sctp, 80/tcp, 9090/tcp
    oai-ausf     /bin/bash /openair-ausf/bi ...   Up (healthy)   80/tcp                      
    oai-ext-dn   /bin/bash -c  ip route add ...   Up                                         
    oai-nrf      /bin/bash /openair-nrf/bin ...   Up (healthy)   80/tcp, 9090/tcp            
    oai-smf      /bin/bash /openair-smf/bin ...   Up (healthy)   80/tcp, 8080/tcp, 8805/udp  
    oai-spgwu    /bin/bash /openair-spgwu-t ...   Up (healthy)   2152/udp, 8805/udp          
    oai-udm      /bin/bash /openair-udm/bin ...   Up (healthy)   80/tcp                      
    oai-udr      /bin/bash /openair-udr/bin ...   Up (healthy)   80/tcp
    [2023-01-22 15:55:19,793] root:DEBUG:  Checking if the containers are configured....
    [2023-01-22 15:55:19,793] root:DEBUG:  Checking if AMF, SMF and UPF registered with nrf core network....
    ...
    [2023-01-22 15:55:19,900] root:DEBUG:  AUSF, UDM, UDR, AMF, SMF and UPF are registered to NRF....
    [2023-01-22 15:55:19,900] root:DEBUG:  Checking if SMF is able to connect with UPF....
    [2023-01-22 15:55:20,014] root:DEBUG:  UPF did answer to N4 Association request from SMF....
    [2023-01-22 15:55:20,070] root:DEBUG:  SMF receiving heathbeats from UPF....
    [2023-01-22 15:55:20,070] root:DEBUG:  OAI 5G Core network is configured and healthy....

indicating that all of the network functions are running and healthy. Note that there is a container deployed called `oai-ext-dn` that will function as an external data network for generating traffic to and from the UE after it attaches to the network. We'll put that to use in a bit, but let's make sure you can ping it now by typing

    ping 192.168.70.135
    
You should see something like the following

    dmaas@node:/opt/oai-cn5g-fed/docker-compose$ ping -c1 192.168.70.135
    PING 192.168.70.135 (192.168.70.135) 56(84) bytes of data.
    64 bytes from 192.168.70.135: icmp_seq=1 ttl=64 time=0.069 ms
    
    --- 192.168.70.135 ping statistics ---
    1 packets transmitted, 1 received, 0% packet loss, time 0ms
    rtt min/avg/max/mdev = 0.069/0.069/0.069/0.000 ms

If not, something has gone wrong and you should raise your hand so a POWDER staff member can come help you out.

Now let's deploy the OAI RAN elements..

#### Build and deploy the OAI 5G RAN components in simulated RF mode

The OAI 5G RAN repository has already been cloned to `/opt/openairinterface5g` on your node. Let's have a look at its contents by typing
    
    cd /opt/openairinterface5g
    ls -l

which will show the following:

    dmaas@node:/opt/oai-cn5g-fed/docker-compose$ cd /opt/openairinterface5g
    dmaas@node:/opt/openairinterface5g$ ls -l
    total 216
    -rwxr-xr-x  1 dmaas mww2023   3946 Jan 16 23:01 CHANGELOG.md
    drwxr-xr-x  3 dmaas mww2023   4096 Jan 16 23:01 charts
    drwxr-xr-x 10 dmaas mww2023   4096 Jan 16 23:01 ci-scripts
    -rw-r--r--  1 dmaas mww2023 110502 Jan 16 23:01 CMakeLists.txt
    drwxr-xr-x  8 dmaas mww2023   4096 Jan 16 23:05 cmake_targets
    drwxr-xr-x  4 dmaas mww2023   4096 Jan 16 23:01 common
    drwxr-xr-x  3 dmaas mww2023   4096 Jan 16 23:01 configuration
    -rw-r--r--  1 dmaas mww2023   1576 Jan 16 23:01 CONTRIBUTING.md
    drwxr-xr-x  7 dmaas mww2023   4096 Jan 16 23:01 doc
    drwxr-xr-x  3 dmaas mww2023   4096 Jan 16 23:01 docker
    drwxr-xr-x  2 dmaas mww2023   4096 Jan 16 23:01 executables
    -rwxr-xr-x  1 dmaas mww2023  12144 Jan 16 23:01 LICENSE
    -rwxr-xr-x  1 dmaas mww2023    283 Jan 16 23:01 maketags
    drwxr-xr-x  4 dmaas mww2023   4096 Jan 16 23:01 nfapi
    -rw-r--r--  1 dmaas mww2023   1093 Jan 16 23:01 NOTICE.md
    -rw-r--r--  1 dmaas mww2023    976 Jan 16 23:01 oaienv
    drwxr-xr-x  9 dmaas mww2023   4096 Jan 16 23:01 openair1
    drwxr-xr-x 19 dmaas mww2023   4096 Jan 16 23:01 openair2
    drwxr-xr-x 17 dmaas mww2023   4096 Jan 16 23:01 openair3
    drwxr-xr-x  2 dmaas mww2023   4096 Jan 16 23:01 openshift
    -rw-r--r--  1 dmaas mww2023   1701 Jan 16 23:01 pre-commit-clang
    drwxr-xr-x 11 dmaas mww2023   4096 Jan 16 23:01 radio
    -rw-r--r--  1 dmaas mww2023   5047 Jan 16 23:01 README.md
    drwxr-xr-x  6 dmaas mww2023   4096 Jan 16 23:01 targets

For reference, here is a description of the directory structure reproduced from the README.md file:

    openairinterface5g
    ├── ci-scripts        : Meta-scripts used by the OSA CI process. Contains also configuration files used day-to-day by CI.
    ├── cmake_targets     : Build utilities to compile (simulation, emulation and real-time platforms), and generated build files.
    ├── common            : Some common OAI utilities, other tools can be found at openair2/UTILS.
    ├── doc               : Contains an up-to-date feature set list and starting tutorials.
    ├── executables       : Top-level executable source files.
    ├── LICENSE           : License file.
    ├── maketags          : Script to generate emacs tags.
    ├── nfapi             : Contains the NFAPI code. A local Readme file provides more details.
    ├── openair1          : 3GPP LTE Rel-10/12 PHY layer / 3GPP NR Rel-15 layer. A local Readme file provides more details.
    │   ├── PHY
    │   ├── SCHED
    │   ├── SCHED_NBIOT
    │   ├── SCHED_NR
    │   ├── SCHED_NR_UE
    │   ├── SCHED_UE
    │   └── SIMULATION    : PHY RF simulation.
    ├── openair2          : 3GPP LTE Rel-10 RLC/MAC/PDCP/RRC/X2AP + LTE Rel-14 M2AP implementation. Also 3GPP NR Rel-15 RLC/MAC/PDCP/RRC/X2AP.
    │   ├── COMMON
    │   ├── DOCS
    │   ├── ENB_APP
    │   ├── F1AP
    │   ├── GNB_APP
    │   ├── LAYER2/RLC/   : with the following subdirectories: UM_v9.3.0, TM_v9.3.0, and AM_v9.3.0.
    │   ├── LAYER2/PDCP/PDCP_v10.1.0
    │   ├── M2AP
    │   ├── MCE_APP
    │   ├── NETWORK_DRIVER
    │   ├── NR_PHY_INTERFACE
    │   ├── NR_UE_PHY_INTERFACE
    │   ├── PHY_INTERFACE
    │   ├── RRC
    │   ├── UTIL
    │   └── X2AP
    ├── openair3          : 3GPP LTE Rel10 for S1AP, NAS GTPV1-U for both ENB and UE.
    │   ├── COMMON
    │   ├── DOCS
    │   ├── GTPV1-U
    │   ├── M3AP
    │   ├── MME_APP
    │   ├── NAS
    │   ├── S1AP
    │   ├── SCTP
    │   ├── SECU
    │   ├── TEST
    │   ├── UDP
    │   └── UTILS
    ├── radio             : drivers for various radios such as USRP, AW2S, RFsim, ...
    └── targets           : Top-level wrappers for unitary simulation for PHY channels, system-level emulation (eNB-UE with and without S1), and realtime eNB and UE and RRH GW.

The first thing we need to do is set up the build environment by typing

    source oaienv

After that, let's switch to the `cmake_targets` sub-directory, where the build utilities exist

    cd cmake_targets

Now, we'll install the necessary dependencies by typing

    ./build_oai -c -C -I --ninja
    
There is a previous build in place in the directory, so we are passing `-c -C` to remove it before we recompile. Also, a lot of the dependencies are already installed on the disk image used for this node in order to save time. (Don't expect this step to complete as fast generally.)

After a few minutes and a lot of output you should see the following:
    
    ...
    -- Configuring done
    -- Generating done
    -- Build files have been written to: /opt/openairinterface5g/cmake_targets/ran_build/build
    10. Bypassing the Tests ...
    BUILD SHOULD BE SUCCESSFUL
    
Now we can start the actual build by typing 

    ./build_oai -c -C --gNB --nrUE -w SIMU --build-lib all --ninja
    
This will take a bit longer (6 minutes or so). While we're waiting, let's open another shell (click on an empty section of desktop and select "XTerm") and start monitoring the output of the `oai-amf` so we can see its interactions with the gNodeB after we start it. Enter this 

    sudo docker logs -f oai-amf
    
We'll pay more attention to that log after we start the gNodeB. Move back to the terminal where the build was progressing, you should eventually see something like this:

    nr-softmodem compiled
    Compiling nr-uesoftmodem...
    Log file for compilation is being written to: /opt/openairinterface5g/cmake_targets/log/nr-ueem.txt
    nr-uesoftmodem compiled
    Building shared libraries common to UE and gNB
    Log file for compilation is being written to: /opt/openairinterface5g/cmake_targets/log/paramnfig.txt
    params_libconfig compiled
    Log file for compilation is being written to: /opt/openairinterface5g/cmake_targets/log/codin
    coding compiled
    Log file for compilation is being written to: /opt/openairinterface5g/cmake_targets/log/telnet
    telnetsrv compiled
    Log file for compilation is being written to: /opt/openairinterface5g/cmake_targets/log/enbsc
    enbscope compiled
    Log file for compilation is being written to: /opt/openairinterface5g/cmake_targets/log/uesco
    uescope compiled
    Log file for compilation is being written to: /opt/openairinterface5g/cmake_targets/log/nrsco
    nrscope compiled
    liboai_device.so is not linked to any device library
    Compiling rfsimulator
    Log file for compilation is being written to: /opt/openairinterface5g/cmake_targets/log/rfsimtxt
    rfsimulator compiled
    Building transport protocol libraries
    Log file for compilation is being written to: /opt/openairinterface5g/cmake_targets/log/oai_espro.txt
    oai_eth_transpro compiled
    liboai_transpro.so is linked to ETHERNET transport
    10. Bypassing the Tests ...
    BUILD SHOULD BE SUCCESSFUL
    
Now we are ready to start the gNodeB...

#### Start the monolithic gNodeB (i.e., no CU/DU/RU split)

Open another shell and start the monolithic gNodeB using the configuration file provided by the profile and appropriate parameters:

    cd /opt/openairinterface5g/cmake_targets
    sudo RFSIMULATOR=server ./ran_build/build/nr-softmodem -O /local/repository/etc/gnb.conf -d --sa --rfsim

The gNodeB options are:

- `-O` to pass config file path
- `-d` to start the gNodeB scope (remote use requires X-forwarding or VNC/similar)
- `--sa` to use 5G `STANDALONE` mode
- `--rfsim` to pass baseband IQ samples via sockets rather than real radio hardware

You will notice the gNodeB connecting to the AMF if you are watching oai-amf log and the gNodeB scope will open up and start showing various plots for the uplink. There won't be anything interesting happening in the scope yet, since we haven't started/attached a UE.

#### Start the UE

Open another shell and start the UE using the configuration file provided by the profile and parameters that match the gNodeB configuration:

    cd /opt/openairinterface5g/cmake_targets
    sudo RFSIMULATOR=127.0.0.1 ./ran_build/build/nr-uesoftmodem -O /local/repository/etc/ue.conf -r 106 -C 3619200000 -d --sa --nokrnmod --numerology 1 --band 78 --rfsim --rfsimulator.options chanmod --telnetsrv

The `nrUE`-specific options are:

- `-r` to set the number of resource blocks
- `-C` to set the center carrier frequency
- `--nokrnmod` to allow UE process to create interface for PDU session
- `--numerology` to set 5G numerology (sub-carrer spacing)
- `--band` to set 3GPP band
- `--rfsimulator.options` to set simulator options (channel modeling in this case)
- `--telnetsrv` to start the telnet server at the UE to allow realtime channel model updates

Note: the UE configuration file sets various credentials (IMSI, Ki, OPC, etc) to match records that exist in the basic OAI 5G core network deployment and includes a channel model configuration file for the RF simulator.

The UE will associate with the network, as indicated by log output from the AMF, gNodeB, and UE processes, and the UE scope will open up showing various plots for the downlink.

If you look again at the gNodeB scope, you'll notice the plots now indicating that there is uplink traffic as well, but it is mostly control-plane traffic.

### Generate some traffic

Let's generate some bi-directional user-plane traffic at the UE by pinging the external data network service (`oai-ext-dn`) that gets deployed along with the OAI 5G core network functions. Open another shell issue the following command.

    ping -I oaitun_ue1 192.168.70.135

Note that we are telling `ping` to use the network interface associated with the UE PDU session. You should see output similar to the following:

```
PING 192.168.70.135 (192.168.70.135) from 12.1.1.130 oaitun_ue1: 56(84) bytes of data.
64 bytes from 192.168.70.135: icmp_seq=1 ttl=63 time=40.2 ms
64 bytes from 192.168.70.135: icmp_seq=2 ttl=63 time=34.7 ms
64 bytes from 192.168.70.135: icmp_seq=3 ttl=63 time=43.4 ms
64 bytes from 192.168.70.135: icmp_seq=4 ttl=63 time=37.4 ms
64 bytes from 192.168.70.135: icmp_seq=5 ttl=63 time=39.2 ms
```

Stop the `ping` process with the key combination `ctrl-C`. We'll reuse the same shell to start an `iperf3` server so we can generate some heavy downlink traffic:

    iperf3 -s

You should see output like the following:

    -----------------------------------------------------------
    Server listening on 5201
    -----------------------------------------------------------

Now open another shell and use it to start an `iperf3` client inside the traffic generation Docker container. We need to point the `iperf3` client at our UE, so we grab the UE IP address and put it in a variable, then use it in the command we execute in the docker container (`oai-ext-dn`).

    UEIP=$(ip -o -4 addr list oaitun_ue1 | awk '{print $4}' | cut -d/ -f1)
    sudo docker exec -it oai-ext-dn iperf3 -c $UEIP -t 50000

You should see output similar to the following:

    Connecting to host 12.1.1.130, port 5201
    [  4] local 192.168.70.135 port 41616 connected to 12.1.1.130 port 5201
    [ ID] Interval           Transfer     Bandwidth       Retr  Cwnd
    [  4]   0.00-1.00   sec  1.17 MBytes  9.79 Mbits/sec    0   76.4 KBytes
    [  4]   1.00-2.00   sec  1.30 MBytes  10.9 Mbits/sec    7    100 KBytes
    [  4]   2.00-3.00   sec  1.43 MBytes  12.0 Mbits/sec    0    110 KBytes
    [  4]   3.00-4.00   sec  1.55 MBytes  13.0 Mbits/sec    0    120 KBytes
    [  4]   4.00-5.00   sec  1.55 MBytes  13.0 Mbits/sec    0    129 KBytes
    ...

Now bring the UE scope back into view (window titled "NR DL SCOPE UE 0"). Notice the changes in the "PDSCH I/Q of MF Output" plot on the UE scope.. the MCS changes to a higher order QAM (more clusters of dots) and the energy plots in the upper sections of the scope show more resource blocks being scheduled. There will probably be 64 clusters apparent (64-QAM).

Note: if you've run into issues getting to this point, you can catch up by running `nohup /local/repository/bin/restart-all.sh` in any shell. It will take a few minutes to stop running processes (if necessary), then start them again.

#### Add some AWGN noise to the downlink

Let's leave `iperf3` running and increase the noise the UE sees in the downlink. To do so, we'll open yet another shell and connect to the telnet server running in the UE softmodem where we can manipulate the channel model.

    telnet 127.0.0.1 9090

You'll see output like this:

    Trying 127.0.0.1...
    Connected to 127.0.0.1.
    Escape character is '^]'.

Press `enter` to yield the following prompt:

  softmodem_5Gue>

We can enter

  channelmod help

to see available options. The output will look like this:

    channelmod commands can be used to display or modify channel models parameters
    channelmod show predef: display predefined model algorithms available in oai
    channelmod show current: display the currently used models in the running executable
    channelmod modify <model index> <param name> <param value>: set the specified parameters in a current model to the given value
                      <model index> specifies the model, the show current model command can be used to list the current models indexes
                      <param name> can be one of "riceanf", "aoa", "randaoa", "ploss", "noise

We started with an `AWGN` model with effectively zero noise in the downlink. We can see the current model by entering

    channelmod show current

which will generate the following output:

    model 0 rfsimu_channel_enB0 type AWGN:
    ----------------
    model owner: rfsimulator
    nb_tx: 1    nb_rx: 1    taps: 1 bandwidth: 0.000000    sampling: 61440000.000000
    channel length: 1    Max path delay: 0.000000   ricean fact.: 0.000000    angle of arrival: 0.000000 (randomized:No)
    max Doppler: 0.000000    path loss: 0.000000  noise: -100.000000 rchannel offset: 0    forget factor; 0.000000
    Initial phase: 0.000000   nb_path: 10
    taps: 0   lin. ampli. : 1.000000    delay: 0.000000
    model 1 rfsimu_channel_ue0 type AWGN:
    ----------------
    model owner: not set
    nb_tx: 1    nb_rx: 1    taps: 1 bandwidth: 0.000000    sampling: 61440000.000000
    channel length: 1    Max path delay: 0.000000   ricean fact.: 0.000000    angle of arrival: 0.000000 (randomized:No)
    max Doppler: 0.000000    path loss: 0.000000  noise: -100.000000 rchannel offset: 0    forget factor; 0.000000
    Initial phase: 0.000000   nb_path: 10
    taps: 0   lin. ampli. : 1.000000    delay: 0.000000

We'll ignore everything but the noise parameter today. Arrange your windows such that the UE scope and the telnet prompt are both visible and keep an eye on the UE scope while we add some noise to the downlink by entering

  channelmod modify 0 noise_power_dB -15

and you'll see the following output from the telnet server indicating the noise parameters has been updated to -15:

    model owner: rfsimulator
    nb_tx: 1    nb_rx: 1    taps: 1 bandwidth: 0.000000    sampling: 61440000.000000
    channel length: 1    Max path delay: 0.000000   ricean fact.: 0.000000    angle of arrival: 0.000000 (randomized:No)
    max Doppler: 0.000000    path loss: 0.000000  noise: -15.000000 rchannel offset: 0    forget factor; 0.000000
    Initial phase: 0.000000   nb_path: 10
    taps: 0   lin. ampli. : 1.000000    delay: 0.000000

You'll notice a bit more noise in the "PDSCH I/Q of MF Output" plot (clusters are little less tight) and the MCS might drop to lower order QAM (fewer clusters, e.g., from 64 to 16).

Let's make the SNR even worse:

    channelmod modify 0 noise_power_dB -5

Now the MCS is almost guaranteed to drop to 16-QAM or 4-QAM. The downlink throughput measured by `iperf3` might drop to zero while the gNodeB figures out that it needs to adjust the MCS to counter the increase in noise.

You can always go back to the setting we started with to remind yourself of the difference.

    channelmod modify 0 noise_power_dB -100

That's it for the OAI 5G RAN/Core handson session!

#### Some other notes

- If you'd like to learn more about the LTE/5G waveform, there is a good set of slides here: https://www.dspcsp.com/tau/5G/05-air-if-2.pdf
- If you want to build the gNode/nrUE with support for SDRs like the USRP, you'll need to use an image with [UHD](https://github.com/EttusResearch/uhd) installed, or use the `build_oai` tool to install it. This is a small adjustments to the instructions we went through. If you want the `build_oai` tool to install UHD for you, do this:

      git clone --branch develop --depth 1 https://gitlab.flux.utah.edu/powder-mirror/openairinterface5g
      cd openairinterface5g
      source oaienv
      cd cmake_targets/
      export BUILD_UHD_FROM_SOURCE=True
      export UHD_VERSION=4.0.0.0
      ./build_oai -I -w USRP --ninja
      ./build_oai --gNB --nrUE -w USRP --build-lib all --ninja

### Moving to "real RF" functionality on POWDER

<img src=https://gitlab.flux.utah.edu/uploads/-/system/personal_snippet/21/958bd9f2aa92db26c602c6568490d0ce/5g-powder.jpg>

Now that you've deployed an E2E OAI 5G network on a single node with simulated RF links, you are better equipped to experiment with 5G profiles that use SDRs that can transmit/receive "real RF" signals. 

There is an overview of options at https://www.powderwireless.net/5g that includes:

- http://powderwireless.net/p/PowderTeam/oai-rf-bench
  
  This profile uses one of our Paired Radio Workbenches to stand up an E2E OAI 5G network in a controlled RF environment. I.e., two X310 SDRs with attenuated wired coax connections.
  
- http://powderwireless.net/p/PowderTeam/oai-indoor-ota

  This profile stands up an E2E OAI 5G network in our Indoor OTA Lab. The UEs are 4x COTS Quectel RM500 modules by default, but you can also use the OAI nrUE in this setup.

- http://powderwireless.net/p/PowderTeam/oai-outdoor-ota (WIP)
  
  This profile stands up an E2E 5G network using Open5GS for the core, OAI for the RAN, and POWDER Dense Endpoints for the gNodeBs. It can be used with COTS UEs on shuttles, but we'll be using a version of it in the next hands on session with portable UE kits.

