## OpenAirInterface 5G - OTA

### Learning objectives

- Understand how to interact with POWDER COTS UEs at a low level (AT commands interface for control and gathering metrics)
- Understand how to attach a COTS UE to a 5G network
- Understand some 5G link metrics
- Gain insight about the realities of outdoor experimentation with open source 5G stacks

### Prerequisites

- Warm clothes!

### Plan

1. Profile, hardware, and software overview (all attendees)
1. Divide into two groups of 30 for parallel activities
    - Group A: 5G OTA activity; then site tour / mini projects
    - Group B: the site tour / mini projects; then 5G OTA activity
1. The group doing the 5G OTA activity will further divide into teams of four,
   which will share a portable UE kit (stand/laptop/UE)
1. POWDER staff will guide you to a site within range of our USTAR dense
   deployment node where each team will...
1. Deploy the portable UE kit try attaching the UE to a 5G network
1. Once the UE is attached, you will use AT commands to capture some link
   metrics like RSRP, RSRQ, SINR, etc.
1. Time permitting, we will move to different location within range of our Wasatch
   dense deployment site and repeat the process.
1. Return to this building and swap groups; repeat
1. Discuss outcomes after both groups have completed the activity

### 1. Profile, hardware, and software overview

#### Profile

- [https://www.powderwireless.net/show/mww2023/5g-ota](https://www.powderwireless.net/show/mww2023/5g-ota)
- Stands up one or more 5G RAN/CN instances using dense endpoints for OAI gNodeBs and fiber-connected server-class compute nodes for Open5GS core networks
- (We've already created an experiment based on this profile for use during this session)
  
#### Hardware

- Each team of 3 will be provided with:
  - 1x computer stand with a COTS UE and antenna attached
  - COTS UE is a Quectel RM520N Module (3GPP Release 16 capable) 
  - Taoglas 5G MIMO antenna
  - 1x POWDER laptop with the necessary drivers and software installed to
    interact with a COTS UE
- (Demonstrate how to set this up)

#### Software (on the provided laptops)

- Ubuntu linux command line environment
- `chat`: tool for interacting with the UE using AT commands
- `quectel-CM`: manages PDU session and IP address/route for the UE
- `ping`/`iperf3`: for traffic generation
- (Demonstrate how to interact with software)
- shortcuts
  - open a terminal by clicking on the terminal icon on the left
  - open new terminals by clicking on File->New Window in any terminal (or use `ctrl-shift-n` while a terminal is focused)
  - use the mouse to move between terminals
  - aliases for important functions
  
### 2,3. Divide into groups and hand out equipment

### 4. Walk to USTAR measurement site

<img src=https://gitlab.flux.utah.edu/uploads/-/system/user/970/0595a28bceb379ec44fbfc77e34f5566/walk-to-ustar.jpg width=500px>

### 5. Setup portable UE kit

  NB: we'll print and hand out instructions. If the long commands are too hard to type in the cold, feel free to use the alternative aliases!
  
  1. Extend stand to comfortable height
  1. Make sure knobs are tight and stand is on level enough ground
  1. Attach UE/antenna mount
  1. Place laptop on stand and power on
  1. Connect UE module to USB port

     <img src=https://gitlab.flux.utah.edu/uploads/-/system/user/970/0f4e056d8da293301c47b69fe9ff70cc/cots-ue-kit.jpg width=500px>

  1. Open a terminal and use an AT command to put the UE in airplane mode
  
         sudo sh -c "chat -t 1 -sv '' AT OK 'AT+CFUN=4' OK < /dev/ttyUSB2 > /dev/ttyUSB2"
         
     or
     
         airplaneon
         
     
     If it throws an error just enter the command again. This command tells `chat` to send the command `AT+CFUN=4` to the AT command serial port for the module and expect `OK` as a response. 
  1. Open a second terminal and start use the following command to start the connection manager `
  
         sudo quectel-CM -s internet -4
         
     or
     
         qconnect

     The output of this utility will show you when the UE sees the gNodeB and tries to attach, as well as the IP address it picks up after a successful attach. 
  1. Wait for POWDER staff member to give the go ahead to turn off airplane mode...
  1. Then open another terminal and type 
         
         sudo sh -c "chat -t 1 -sv '' AT OK 'AT+CFUN=1' OK < /dev/ttyUSB2 > /dev/ttyUSB2"
         
     or
     
         airplaneoff
         
     to disable airplane mode (turn the radio back on). Hopefully, the UE will see a cell with PLMN **99999** and attach. If so, you'll see some action in the connection manager output indicating as much, but it doesn't always work immediately or on the first try. Ask a POWDER team member for help if/when you run into issues.
  1. If the UE successful attaches, you should see `quectel-CM` output something like the following, indicating you have attached to the cell and have an IP address:

         [01-18_14:05:51:962] requestRegistrationState2 MCC: 999, MNC: 99, PS: Detached, DataCa     p: UNKNOW
         [01-18_14:05:51:994] requestRegistrationState2 MCC: 999, MNC: 99, PS: Detached, DataCa     p: UNKNOW
         [01-18_14:05:52:026] requestRegistrationState2 MCC: 999, MNC: 99, PS: Detached, DataCa     p: UNKNOW
         [01-18_14:06:02:907] requestRegistrationState2 MCC: 999, MNC: 99, PS: Attached, DataCa     p: 5G_SA
         [01-18_14:06:03:067] requestSetupDataCall WdsConnectionIPv4Handle: 0x4793ef70
         [01-18_14:06:03:196] change mtu 1500 -> 1400
         [01-18_14:06:03:196] ifconfig wwan0 up
         [01-18_14:06:03:213] ifconfig enx922227582aa7 up
         [01-18_14:06:03:236] busybox udhcpc -f -n -q -t 5 -i enx922227582aa7
         udhcpc: started, v1.30.1
         udhcpc: sending discover
         udhcpc: sending select for 10.45.0.2
         udhcpc: lease of 10.45.0.2 obtained, lease time 7200
         [01-18_14:06:03:443] /etc/udhcpc/default.script: Adding DNS 8.8.8.8
         [01-18_14:06:03:443] /etc/udhcpc/default.script: Adding DNS 8.8.4.4
         
     The network interface and IP address may be different than what you see here. Typing 
     
         ip a
         
     into another terminal should show the IP for that interface as well, e.g.: 

         6: enx922227582aa7: <NOARP,UP,LOWER_UP> mtu 1400 qdisc fq_codel state UP group default qlen 1000
             link/ether 92:22:27:58:2a:a7 brd ff:ff:ff:ff:ff:ff
             inet 10.45.0.2/29 scope global enx922227582aa7
                valid_lft forever preferred_lft forever
             inet6 fe80::5308:b5da:a662:a7b0/64 scope link noprefixroute
                valid_lft forever preferred_lft forever
      
### 6. Capture some link metrics 

  1. Run 
  
         getmetrics
         
     to see some link metrics for the cell. You should see output similar to this:
  
         MCC: 999
         MNC: 99
         cellID: E00
         PCID: 1
         RSRP: -75
         RSRQ: -1
         SINR: 1

     In particular, we are interested in:
     
     - RSRP: reference signal received power (dBm)
       - average power of reference signals
     - RSRQ: reference signal received quality (dB)
       - This is the (number of PRBs) * RSRP / RSSI
       - RSSI: total power including signal, noise, and interference across the whole bandwidth
       - Always negative
       - Similar to SINR, but dependent on PRB allocation
     - SINR: signal to interference and noise power ratio (dB)
       - estimated based on power per resource element, reference signal or otherwise
       - does not depend on bandwidth or PRB allocations 
     
     | RF Conditions | RSRP (dBm)  | RSRQ (dB)  | SINR (dB) |
     |---------------|-------------|------------|-----------|
     | Excellent     | > -80       | > -10      | > 20      |
     | Good          | -80 to -90  | -10 to -15 | 13 to 20  |
     | Mid Cell      | -90 to -100 | -15 to -20 | 0 to 13   |
     | Cell Edge     | < -100      | < -20      | < 0       |
     
     These values will show some variance as the environment changes because of fading, shadowing, noise, etc. The gNodeB MAC/Scheduler will adapt the modulation and coding scheme based on the current RF conditions.
     **Make a note of the RSRP/RSRQ/SINR values you see at each position, how they change over time. We will discuss this when we get back.**
     

  1. While your UE is attached, try pinging the core network node with `ping 10.45.0.1`.
     **Make a note of the round trip time.. how much does it vary?**. 
     
  1. If we have time, we will start an `iperf3` server at the core network node so you can generate some more significant downlink traffic with:
  
         iperf3 -c 10.45.0.1 -R
        
     This command generates TCP traffic in the downlink direction. We may also try creating UDP traffic with, e.g.:
     
         iperf3 -c 10.45.0.1 -R -u -b 50M
         
     which tells the server to generate downlink traffic at 50 Mbps.
     **Make a note of the downlink throughput you see if we get to this.**. 

  NB:
  
  - The UEs and the channel can be fickle! Your UE may fail to attach for a while and then attach repeatedly without issue with no apparent change in your position or the general environment!
  - The open source 5G stacks in use are not commercial grade yet. Either or both may crash multiple times during this activity. We'll have a POWDER staff member monitoring these processes and restarting them if necessary during the session.

### 7. Move to the next location and repeat the process!

<img src=https://gitlab.flux.utah.edu/uploads/-/system/user/970/7524b2da127d097d3dae42bb500dc459/walk-to-wasatch.jpg width=500px>

### 8. Return to Alumni House and discuss observations

  - Were you able to attach your UE?
  - What sort of channel metrics did you see at both/either location?
    - How were they different?
    - Any thoughts about why?
