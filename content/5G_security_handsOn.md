### 5G OAI (OpenAirInterface) : POWDER OTA (OverTheAir) Lab

## Learning objectives
- Understand how to deploy 5G-OAI-basic / gNB / UE in POWDER indoor OTA Lab
- Take trace
- Analyze 5G initial registration and authentication procedure from the trace

## Pre-requiste
- Installed Wireshark in your laptop

## Hands-on walkthrough instructions

# Instantiating experiment
Login to POWDER portal: [POWDER](https://www.powderwireless.net/) 

From top menu select `Experiments --> Start Experiment`

Clicking the `Change Profile` button will let you select the profile that your experiment will be built from

In this hands on sessin we will use `oai-indoor-ota` profile (from left pannel) and click `Select Profile`

Click on the `Show profile` button, to view the profile details.

Scroll down at the bottom of the page, and `instantiate` the `test-basic-core` branch. 

This profile instantiates an experiment for testing OAI 5G with COTS UEs in standalone mode using resources in the POWDER indoor over-the-air (OTA) lab. The indoor OTA lab includes:

- A d430 compute node to host the core network
- A d740 compute node for the gNodeB
- One of the four indoor OTA X310s
- All four indoor OTA NUCs

Click next.

Select the parameters. This profile requires 2 compute nodes. 1 for the gNB (d740) and the other for CN (d430). For USRP we will use X310 and select a frequency range.

Note: This profile currently requires the use of the 3550-3600 MHz spectrum range and you need an approved reservation for this spectrum in order to use it. It's also strongly recommended that you include the following necessary resources in your reservation to gaurantee their availability at the time of your experiment:

You can find a diagram of the lab layout here: [OTA Lab diagram](https://gitlab.flux.utah.edu/powderrenewpublic/powder-deployment/-/raw/master/diagrams/ota-lab.png)

Type the `name` (of your experiment) and select the `project` to work in and click `Next`. For today's session selection the projet `mww2023`.

Provisioning and booting will start and take some time to complete. Startup scripts will still be running when your experiment becomes ready. Watch the "Startup" column on the "List View" tab for your experiment and wait until all of the compute nodes show "Finished" before proceeding.

# 5G-CN (basic deployment)

In this session we will require at least 6 separate terminal window session. `tmux` can also be used if you want to manage from a single terminal window.

- Open 3 separate ssh sessions to `cn` node:   
    - Session 1: Start 5G core-network services. It will take several seconds for the services to start up. Make sure the scripts indicates that the services are healthy before moving on.
    ```
    cd /var/tmp/oai-cn5g-fed/docker-compose
    sudo python3 ./core-network.py --type start-basic --scenario 1
    ```
    - Session 2: Start following the logs for the AMF. This way you can see when the UE syncs with the network.
    ```
    sudo docker logs -f oai-amf
    ```
    - Session 3: To monitor traffic between the various network functions, gNodeB and UE, start tcpdump in a session. You can use a specific destination to save the .pcap file. *Change the below command with your userName and fileName.
    ```
    sudo tcpdump -i demo-oai   -f "not arp and not port 53 and not host archive.ubuntu.com and not host security.ubuntu.com" -w /users/sayazm/5GTraceAnalysis.pcap
    ``` 

- On a `gNB` session :
```
sudo /var/tmp/oairan/cmake_targets/ran_build/build/nr-softmodem -E   -O /var/tmp/etc/oai/gnb.sa.band78.fr1.106PRB.usrpx310.conf --sa
```
After you've started the gNodeB, you can bring the COTS UE online. 

- Open 2 `ota-nucX` sessions:
    - Session 1: Start the Quectel connection manager:
    ```
    sudo quectel-CM -s oai -4
    ```
    - Session 2: Bring the UE online:
    ```
    # turn modem off
    sudo modemctl airplane-mode

    # turn modem on
    sudo modemctl full-functionality
    ```
    The UE should attach to the network and pick up an IP address on the wwan interface associated with the module. You'll see the wwan interface name and the IP address in the stdout of the quectel-CM process.

You should now be able to generate traffic in either direction:
```
# from UE to CN traffic gen node (in session on ota-nucX)
ping 192.168.70.135

# from CN traffic generation service to UE (in session on cn node)
sudo docker exec -it oai-ext-dn ping <IP address from quectel-CM>
```
# Trace Analysis
To transfer file from CN node to your machine
```
scp userName@pcxxx.emulab.net:/users/sayazm/5GTraceAnalysis.pcap /Users/ayaz/Desktop/
```

# To add encryption method
Add following parameters in oai-amf service of docker-compose-basic.yaml and restart the basic deployment
```
    - INT_ALGO_LIST=["NIA1" , "NIA2"]
    - CIPH_ALGO_LIST=["NEA1" , "NEA2"]
```

