### Notes for MWW attendees

- The MWW week sessions are heavily hands-on based. Please bring your own laptop with charger.
- For most hands-on sessions we will make use of browser-based VNC remote desktop functionality. Based on our experience this works best with the Google Chrome browser. Please have Chrome installed on your laptop ahead of time. 
- We will make use of the POWDER "mww2023" project. Please join the project before the start of MWW:
    - If you already have an account on POWDER:
        - Log into [https://www.powderwireless.net](https://www.powderwireless.net) 
        - Click on your name in the top right and select "Start/Join Project"
        - Select "Join Existing Project" and enter: mww2023
        - Click on "Submit Request"
    - If you do not have a POWDER account:
        - Go to [https://www.powderwireless.net](https://www.powderwireless.net)
        - Click on "Request an Account"
        - Complete the details on the left
        - On the right select "Join Existing Project" and enter: mww2023 
        - Click on "Submit Request"
    - POWDER staff will process these requests before the start of MWW
- This repo will be your main starting point for all MWW sessions/material. We will continue to update/add to this as we go.
- Please don't request access to material that is not yet "visible", we will change the access permissions when it is ready.
- During MWW the POWDER-RENEW team members will largely be present during hands-on sessions so you can ask them directly when you get stuck. That said, we have set up a slack channel for attendees where you can also ask for help.
- We will make use of [Wireshark](https://www.wireshark.org) for the 5G Security hands-on session. Please install that on your laptop if you don't have it already.
- We will make use of python on your laptop (or someone in your group's laptop) for the Interference Localization session on Tuesday.  We use the [Anaconda Distribution](https://www.anaconda.com/products/distribution) for python.  We have tested our code on python versions 3.7 and 3.8.  If possible, please download and install it on your laptop.

<!---
- This repo will be made public before the event.
- Please put all your content in the "content" folder.
- For "Hands-on" sessions: Try to articulate "platform related" and "domain specific" learning goals
- For Google documents (e.g., Google presentations), please use this [shared folder](https://drive.google.com/drive/folders/1rwrMXn_7OR0VegyNB8ME1gRRPHOynSdU?usp=sharing). (That folder will be made public before the event.)
- We will use the "mww2023" POWDER project, please join that and make sure your profiles are visible in that. 
- The detailed schedule document with tasks, etc. is here: [Detailed Schedule and Tasks](https://docs.google.com/document/d/1PUUHEbVKzCE6e_Ur4Sf67-JBwilCh_AjvFvKRGJhpSU/edit?usp=sharing)
--->

# POWDER-RENEW Mobile and Wireless Week 2023 (MWW2023)

POWDER-RENEW Mobile and Wireless Week is scheduled for the week of January 23rd through 27th on the University of Utah campus in Salt Lake City Utah.

We will be meeting in the [Henriksen Conference Room](https://ecclesalumnihouse.utah.edu/spaces/henriksen-conference-room/) in the [Cleone Peterson Eccles Alumni House](https://ecclesalumnihouse.utah.edu). 

If you need parking on campus this [map](https://commuterservices.utah.edu/wp-content/uploads/sites/22/2021/08/guidemap_maplist_27jul21.pdf) shows the location of (_pay for_) visitor parking lots. (The Alumni House is number 52 on the map.)

Please note that on Tuesday afternoon we are planning a tour of the POWDER facility and an outdoor hands-on session. These plans are of course weather dependent, but you will want to bring appropriate warm clothing and wear comfortable walking shoes for these sessions. (We are planning to have an indoor alternative for the hands-on session, in case of bad weather, or for those who might not want to spend time outside.)

## Schedule

### Monday

* 8:30am - 10:00am *POWDER Overview and experimental workflow*

    * P1: Presentation: [POWDER Overview](https://docs.google.com/presentation/d/1vlz5K7vEg9tyMpJI5gxd3_q1kkNtRu0sCZ4BhOz0eIo/edit?usp=sharing) (*Kobus Van der Merwe*)
    * H1: Hands-on: [POWDER experimental workflow & Spectrum monitoring](https://gitlab.flux.utah.edu/powderrenewpublic/mww2023/-/blob/main/content/ota_monitor.md) (*Kobus Van der Merwe*)

* 10:00am - 10:30am Break

* 10:30am - noon *Communications Basics - 1*

    * P2: Presentation: [Power, Path loss, Multipath and Shadow fading](https://gitlab.flux.utah.edu/powderrenewpublic/mww2023/-/blob/main/content/patwari_notes_mww2023.pdf) (*Neal Patwari*)
    * H2: Hands-on: [Path loss measurements & Shout measurement framework](https://gitlab.flux.utah.edu/powderrenewpublic/mww2023/-/blob/main/content/path_loss_shout.md) (*Kirk Webb*)

* noon - 1:30pm Lunch (on your own)

* 1:30pm - 3:00pm *Communications Basics - 2*

    * P3: Presentation: [QAM/PSK Modulation](https://gitlab.flux.utah.edu/powderrenewpublic/mww2023/-/blob/main/content/patwari_notes_mww2023.pdf) (*Neal Patwari*)
    * H3: Hands-on: [Narrowband modulation (over-the-air)](https://github.com/npatwari/tx_rx_processing/blob/main/README.md) (*Neal Patwari*)
        * Data collected during the hands-on session is [here](https://gitlab.flux.utah.edu/powderrenewpublic/mww2023/-/blob/main/data/Shout_meas_01-23-2023_14-31-20.zip)

* 3:00pm - 3:30pm Break

* 3:30pm - 5:00pm Mini-project presentation/discussion

    * [Plan with mini-projects](https://docs.google.com/presentation/d/1Vmi1CJEl_v2V38GjtYinA9kuLz3AUED1AyRhraV2W6k/edit?usp=sharing)
    * [Mini project ideas](https://docs.google.com/document/d/1cN_aAe3P1qkez11eYo7RibmljqeiPQMy26zV0ULhK-0/edit?usp=sharing)
    * [Spreadsheet for mini project interest](https://docs.google.com/spreadsheets/d/1Qf58H2PIZSonlKd6PIxerP7Dx8j9q0gW5IbSVc9la9w/edit?usp=sharing)

### Tuesday

* 8:30am - 10:00am *Localization*

    * P4: Presentation: [Interference Localization via Time Difference of Arrival (TDoA)](https://docs.google.com/presentation/d/10apfFgf24SryTKmz0VFWEHd7DEwoXKpNh2NABKCo6TY/edit?usp=sharing) (*Neal Patwari*)
    * H4: Hands-on: [Interference source localization via TDOA](https://gitlab.flux.utah.edu/npatwari/interference_source_localization/-/blob/main/README.md) (*Neal Patwari*)

* 10:00am - 10:30am Break

* 10:30am - noon *Mobile Networking - 1* 

    * P5: Presentation: [Mobile Networking](https://docs.google.com/presentation/d/1faOENw7xJuuSfPgf4ObmISvadJk2kNlMc66Wnsf2zmU/edit?usp=sharing) (*Dustin Maas*)
    * H5: Hands-on: [OpenAirInterface 5G - RAN/Core](https://gitlab.flux.utah.edu/powderrenewpublic/mww2023/-/blob/main/content/oai.md) (Dustin Maas)

* noon - 1:30pm Lunch (on your own)

* 1:30pm - 5:00pm *Mobile Networking - 2 & POWDER Tour (parallel sessions)* 

    * H6: Hands-on: [OpenAirInterface 5G - OTA](https://gitlab.flux.utah.edu/powderrenewpublic/mww2023/-/blob/main/content/oai_ota.md) (*Dustin Maas*)
    * POWDER platform tour & Work on mini-projects

### Wednesday

* 8:30am - 10:00am *5G security*

    * P7: Presentation: [5G Security](https://drive.google.com/file/d/1fZ6fUUiNOVjf8I9IdZAdp2s9RLjZiVUE/view?usp=share_link) (*Sneha Kasera*)
    * H7: Hands-on: 5G Security messaging [Slides](https://docs.google.com/presentation/d/1tBY8s0aXunRHEwdTYzzim56DvGKNX1EA/edit?usp=sharing&ouid=107755707671414140102&rtpof=true&sd=true) [Instructions](https://gitlab.flux.utah.edu/powderrenewpublic/mww2023/-/blob/main/content/5G_security_handsOn.md)  (*Ayaz Mahmud*)

* 10:00am - 10:30am Break

* 10:30am - noon *O-RAN - 1*

    * P8: Presentation: [Open RAN Overview](https://docs.google.com/presentation/d/1cr9nZXAIAvxKOzHLQmxGSGpOJ-6147KYO3k3wmu3gb8/edit?usp=sharing)(*David Johnson*)
    * H8: Hands-on: [O-RAN - Near RT RIC](https://gitlab.flux.utah.edu/powderrenewpublic/mww2023/-/blob/main/content/oran.md) (*David*)
    

* noon - 1:30pm Lunch (on your own)

* 1:30pm - 3:00pm *O-RAN - 2*

    * H9: Hands-on: [O-RAN - "top-to-bottom" stack change](https://gitlab.flux.utah.edu/powderrenewpublic/mww2023/-/blob/main/content/oran_modify.md) (*David Johnson*)

* 3:00pm - 3:30pm Break

* 3:30pm - 5:00pm Work on mini-projects

### Thursday
[Massive Mimo P10-P12 Presentation](https://rice.box.com/s/6a7zvexusho6fkssagpzxigsr0zsc1p4)
* 8:30am - 10:00am Massive MIMO - RENEW Basics

    * P10: Presentation: Basics of Multi-Antenna Communication (*Nico Barati*)
    * H10: Hands-on: [RENEWLAB, SoapySDR + Matlab + Python](https://wiki.renew-wireless.org/en/architectures/softwarearchitectureoverview) (*Oscar Bejarano*)

* 10:00am - 10:30am Break

* 10:30am - noon Massive MIMO - Channel Measurements using RENEWLab
    * P11: Presentation: Channel Measurements
    * H11: Hands-on: [Channel Measurements (RENEWLab - Sounder)](https://wiki.renew-wireless.org/en/quickstartguide/MobileWirelessWeek2023#sounder-hands-on-experiments-h11) (*Andrew Sedlmayr / Rahman Doost-Mohammady*)

* noon - 1:30pm Lunch (on your own)

* 1:30pm - 3:00pm Massive MIMO - Realtime Phy - Agora 1.0
    * P12: Presentation:  Introduction to Agora 1.0
    * H12: Hands-on: [Realtime PHY - Agora 1.0](https://wiki.renew-wireless.org/en/quickstartguide/MobileWirelessWeek2023#agora-hands-on-experiments-h12) (*Andrew Sedlmayr / Rahman Doost-Mohammady / Di Mu*)

* 3:00pm - 3:30pm Break

* 3:30pm - 5:00pm Work on mini-projects

### Friday

* 8:30am - 10:00am Work on mini-projects

* 10:00am - 10:30am Break

* 10:30am - noon Work on mini-projects

* noon - 1:30pm Lunch (on your own)

* 1:30pm - 3:00pm Work on mini-projects

* 3:00pm - 5:00pm Mini-project presentations/demo videos
