## RF Modulation

### Learning objectives

- Understand how to use PySDR (??) to perform low level RF experiments
- Understand how to perform narrowband RF modulation over-the-air in POWDER
- Understand LoRa communication 
- Understand how to use artifacts created by others in your POWDER experiments

Resources:
- Rooftops: 8 (with WR)
- Dense: 2 + ?? (with WR)
