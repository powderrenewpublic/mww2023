## POWDER experimental workflow & Spectrum monitoring

### Learning objectives:

- Understand commonly used POWDER experimental workflows and POWDER platform terminology
- Understand how to create/modify POWDER profiles and how profiles specify the resources to be used in an experiment
- Understand the heterogeneity of POWDER radio resources and that different resources support different research goals
- Understand that spectrum is a critical POWDER resource and that spectrum use within POWDER happens in a real world environment where there are other spectrum users
- Understand how to perfrom basic spectrum monitoring activities on POWDER

### Session overview

1. Attendees divided into groups (based on resource availability).
2. Each group creates a new profile (requesting resources for one SDR) and instantiates an experiment with that SDR (pay attention to profile/experiment naming instructions).
3. Use the FCC spectrum database to look up details about CBRS and C-band spectrum.
4. Use your group's SDR experiment to see if you can detect activity assoiated with the frequency ranges.
5. (Optional) Repeat some of the measurement activities using a different type of radio, or a radio at a different location.
5. (Optional) Follow instructions to create a parameterized profile that can be used to allocate radios associated with this activity.

### Create a POWDER experiment with one radio

#### Per-group information

<!---
"bookstore","nuc1": works
"bookstore","nuc2": works (poor signal)
"ebc","nuc1": works
"ebc","nuc2": ??
"cpg","nuc1": works
"cpg","nuc2": works
"guesthouse","nuc1": works
"guesthouse","nuc2": works
"humanities","nuc1": works
"humanities","nuc2": works
"law73","nuc1": works
"law73","nuc2": works
"madsen","nuc1": works but not well
"madsen","nuc2": works
"moran","nuc1": likely works (faint signal 2310)
"moran","nuc2": works (2310), doesn't see 3720
"sagepoint","nuc1": works
"sagepoint","nuc2": works
"web","nuc1": works (poor backhaul?)
"web","nuc2": not working

cnode-ebc: works
cnode-guesthouse: works
cnode-mario: works
cnode-wasatch: works
moran?
ustar?

cbrssdr1-bes: works
cbrssdr1-honors: works
cbrssdr1-hospital: works
cbrssdr1-smt: works
cbrssdr1-ustar: works
cbrssdr1-browning: works
cbrssdr1-fm: works
dentistry?
meb?


cellsdr1-hospital: works
cellsdr1-bes: works 
smt?: not working

-->

| Group | Function  | Parameters | Profile Name/Experiment Name |
| --- | --- | --- | --- |
| 1 | fixedendpoint_nuc_pair | "ebc","nuc1" | fixed-ebc-nuc1 |
| 2 | fixedendpoint_nuc_pair | "cpg","nuc1" | fixed-cpg-nuc1 |
| 3 | fixedendpoint_nuc_pair | "guesthouse","nuc1" | fixed-guesthouse-nuc1 |
| 4 | fixedendpoint_nuc_pair | "humanities","nuc1" | fixed-humanities-nuc1 |
| 5 | fixedendpoint_nuc_pair | "law73","nuc1" | fixed-law73-nuc1 |
| 6 | fixedendpoint_nuc_pair | "sagepoint","nuc1" | fixed-sagepoint-nuc1 |
| 7 | fixedendpoint_nuc_pair | "web","nuc1" | fixed-web-nuc1 |
| 8 | fixedendpoint_nuc_pair | "cpg","nuc2" | fixed-cpg-nuc2 |
| 9 | fixedendpoint_nuc_pair | "guesthouse","nuc2" | fixed-guesthouse-nuc2 |
| 10 | fixedendpoint_nuc_pair | "humanities","nuc2" | fixed-humanities-nuc2 |
| 11 | fixedendpoint_nuc_pair | "law73","nuc2" | fixed-law73-nuc2 |
| 12 | fixedendpoint_nuc_pair | "madsen","nuc2" | fixed-madsen-nuc2 |
| 13 | fixedendpoint_nuc_pair | "sagepoint","nuc2" | fixed-sagepoint-nuc2 |
| 14 | dense_nuc_pair | "cnode-ebc" | dense-ebc |
| 15 | dense_nuc_pair | "cnode-guesthouse" | dense-guesthouse |
| 16 | dense_nuc_pair | "cnode-mario" | dense-mario |
| 17 | dense_nuc_pair | "cnode-moran" | dense-wasatch |
| 18 | rooftop_node_pair | "cbrssdr1-bes","d740" | rooftop-cbrs-bes |
| 19 | rooftop_node_pair | "cbrssdr1-honors","d740" | rooftop-cbrs-honors |
| 20 | rooftop_node_pair | "cbrssdr1-hospital","d740" | rooftop-cbrs-hospital |
| 21 | rooftop_node_pair | "cbrssdr1-smt","d740" | rooftop-cbrs-smt |
| 22 | rooftop_node_pair | "cbrssdr1-ustar","d740" | rooftop-cbrs-ustar |
| 23 | rooftop_node_pair | "cbrssdr1-browning","d740" | rooftop-cbrs-browning |
| 24 | rooftop_node_pair | "cbrssdr1-fm","d740" | rooftop-cbrs-fm |
| 25 | rooftop_node_pair | "cellsdr1-hospital","d740" | rooftop-cell-hospital |
| 26 | rooftop_node_pair | "cellsdr1-bes","d740" | rooftop-cell-bes |
| 27 | rooftop_node_pair | "cellsdr1-smt","d740" | rooftop-cell-smt |

#### Create a profile for spectrum monitoring

1.  Log into the POWDER portal. Find the `mww2023_ota_monitor` profile. (Click on `Experiments`, select `Start Experiment`, click on `Change Profile` and enter `mww2023_ota_monitor` in the search box.) Or, click [here](https://www.powderwireless.net/p/mww2023/mww2023_ota_monitor).

2. Rather than _instantiating_ the profile, make your own copy by selecting the `Copy Profile` option. **Use the `Profile Name` entry for your group in the table above for the new profile.**

3. Edit the new profile and add the appropriate function call and parameters listed in the table above.

	For example, group 3 will add:
	
	```
	fixedendpoint_nuc_pair("guesthouse","nuc1")
	```
	And group 16 will add:

	```
	dense_nuc_pair("cnode-mario")
	```

4.  Save the profile and click `Instantiate` to instantiate the profile. **On the `Finalize` page be sure to use the `Experiment Name` for your group listed in the table above.**

5.  Once your experiment is instantiated, verify that your compute node can interact with the SDR/USRP.
	- Click on the "List View" tab of your experiment. (If you no longer have your experiment view in your browser you can find it again by clicking on the "Experiments" drop down in the POWDER portal, selecting "My Experiments" and then clicking on the experiment name in the resulting table.)
	- Click on the "gear icon" in the right hand column in the row associated with your node and select "Open VNC window" from the dropdown menu. This will result in a VNC browser window being opened on the Linux-based compute node in your experiment.
	- In an xterm within the VNC window, execute the following commands to verify that the SDR/URSP is visible/operational. 
	```
	uhd_find_devices
	uhd_usrp_probe
	```

### FCC spectrum license lookup and measurement

#### FCC spectrum database search

1. Find the frequency ranges used by 5G NR band n48. (CBRS) E.g., from here:
   
   [https://en.wikipedia.org/wiki/5G_NR_frequency_bands](https://en.wikipedia.org/wiki/5G_NR_frequency_bands)

<!--
	3550 - 3700 MHz
-->

2. Find the frequency range used by 5G NR band n78. (C-Band)
<!--
	3300 - 3800 MHz
-->
Since these frequency ranges overlap, for our purposes we will consider that as three separate bands:
- Lower C-band
- CBRS
- Higher C-band
(Note that in the US C-band often refers to a slightly different range: [https://www.pcmag.com/news/what-is-c-band](https://www.pcmag.com/news/what-is-c-band))

<!--
- Lower C-band: 3300-3550
- CBRS: 3550-3700
- Higher C-band: 3700-3800
-->

_POWDER currently can make use of "Lower C-band" and the lower part of the CBRS band._


3. Point your browser at the FCC spectrum database search page:
	
	[https://wireless2.fcc.gov/UlsApp/UlsSearch/searchLicense.jsp](https://wireless2.fcc.gov/UlsApp/UlsSearch/searchLicense.jsp)
	
4. Select the `Geographic` search option.

5. Enter the following search criteria:

	(a) State: Utah

	(b) County: UT - Salt Lake

	(c) Enter a frequency range that covers the Higher C-band frequency range.

<!--
WRNE782: 
PEA027  - Salt Lake City, UT 
Channel Block	A1 
Associated Frequencies (MHz)	003700.00000000-003720.00000000
WRNE783:
PEA027  - Salt Lake City, UT
Channel Block	A2
Associated Frequencies (MHz)	003720.00000000-003740.00000000 
WRNE784:
PEA027  - Salt Lake City, UT 
Channel Block	A3  
Associated Frequencies (MHz)	003740.00000000-003760.00000000 
WRNE785:
PEA027  - Salt Lake City, UT 
Channel Block	A4 
Associated Frequencies (MHz)	003760.00000000-003780.00000000
WRNE786:
PEA027  - Salt Lake City, UT 
Channel Block	A5
Associated Frequencies (MHz)	003780.00000000-003800.00000000 
WRNJ279: (Interim)
PEA027  - Salt Lake City, UT 
Channel Block	A4
Associated Frequencies (MHz)	003760.00000000-003780.00000000
>> Linked to WRNJ278: 003860.00000000-003880.00000000
WRNJ281: (Interim)
PEA027  - Salt Lake City, UT
Channel Block	A5
Associated Frequencies (MHz)	003780.00000000-003800.00000000 
>> Linked to WRNJ280: Associated Frequencies (MHz)	003880.00000000-003900.00000000 
-->

6. Explore the results (focus on "Active" licenses that cover the "Salt Lake City" market):
	
	(a) What channels and frequency ranges are covered by each license?
	
	(b) Anything odd about coverage?

<!--
Verizon and AT&T cover same range for some subset.
The AT&T licnese are marked as "Interim" and are linked to licenses 1GHz higher. Maybe related to the altimeter concerns: https://www.aviationtoday.com/2022/03/24/the-latest-5g-c-band-interference-on-radio-altimeters-research-testing-and-technology-updates/ 
-->

#### Spectrum measurement (high C-band) using uhd_fft

| Type of node | uhd_fft command | Example |
| --- | --- | --- |
| Fixed endpoint - nuc1 | uhd_fft -A RX2 -s <SAMPLE_RATE> -f <CENTER_FREQUENCY> | `uhd_fft -A RX2 -s 50M -f 2310M` |
| Fixed endpoint - nuc2 | uhd_fft -A TX/RX -s <SAMPLE_RATE> -f <CENTER_FREQUENCY> | `uhd_fft -A TX/RX -s 50M -f 2310M` |
| Rooftop - cbrs | uhd_fft -A TX/RX -s <SAMPLE_RATE> -f <CENTER_FREQUENCY> | `uhd_fft -A TX/RX -s 100M -f 3560M` |
| Rooftop - cellsdr1-smt (with Keysight antenna) | uhd_fft -A TX/RX -s <SAMPLE_RATE> -f <CENTER_FREQUENCY> | `uhd_fft -A TX/RX -s 100M -f 2310M` |
| Rooftop - cellsdr1-* (with Keysight antenna) | uhd_fft -A RX2 -s <SAMPLE_RATE> -f <CENTER_FREQUENCY> | `uhd_fft -A RX2 -s 100M -f 2310M` |
| Dense | uhd_fft -A TX/RX -s <SAMPLE_RATE> -f <CENTER_FREQUENCY> | `uhd_fft -A TX/RX -s 50M -f 3560M` |

1. *Representative from each group* log into the compute node associated with your SDR experiment. (I.e., folow the instructions above to open a VNC browser window on the node.) Run `uhd_fft` in an xterm to try to determine whether these Higher C-band licenses are in use.

	(a) Select a center frequency "close" to the range you want to observe.

	(b) Execute `uhd_fft` with the appropriate options based on the table above.
	
	Note: You might have to run uhd_fft a number of times with different parameters to properly the complete frequency ranges. (Or, adjust it from within the GUI.)
	
	
2. Explore the results:

	(a) Is there activity in the frequency range?

	(b) Any other noteworthy activity?	

#### More spectrum license exploration (CBRS band)

The CBRS band is "interesting" in that it follows a "dynamic spectrum sharing" approach to issuing licenses (as opposed to static license allocation used in most other bands). There is a lot of information about CBRS available, e.g., this [brief summary from FIERCE Wireless](https://www.fiercewireless.com/private-wireless/what-cbrs) and this [overview from the FCC](https://www.fcc.gov/wireless/bureau-divisions/mobility-division/35-ghz-band/35-ghz-band-overview).  

1. Follow a similar geographic FCC search approach to verify that there are no (or few) active licenses in the CBRS range for our location.

- Point your browser at the FCC spectrum database search page:
	
	[https://wireless2.fcc.gov/UlsApp/UlsSearch/searchLicense.jsp](https://wireless2.fcc.gov/UlsApp/UlsSearch/searchLicense.jsp)
	
- Select the `Geographic` search option.

- Enter the following search criteria:

	(a) State: Utah

	(b) County: UT - Salt Lake

	(c) Enter a frequency range that covers the CBRS frequency range.

- Explore the results (focus on "Active" licenses that cover the "Salt Lake City" market):
	
	- For the active licenses resulting from this search, notice the listed expiration dates. These are likely "grandfathered" users of the band.


2. Perfrom similar spectrum monitoring activities for the CBRS frequency range to see if there is any activity. 

- Due to a variety of reasons (location, height, antenna gain etc.) the different radios will vary in their ability to show RF activity in this range (rooftop-cbrs > rooftop-cell > dense >fixed). You might have to run uhd_fft longer and activate "Max Hold" to identify activity.




<!--
Notes

[draft profile](https://www.powderwireless.net/p/mww2023/mww2023_ota_monitor)

Sanity check:

- Rooftop: 
    - CBRS: uhd_fft -A TX/RX -s 100M -f 3560M 
    - Keysight connected cell: uhd_fft -A RX2 -s 100M -f 2310M

- Dense: uhd_fft -A TX/RX -s 50M -f 3560M

- Fixed-endpoint:
 
    - nuc1: uhd_fft -A RX2 -s 50M -f 2310M
    - nuc2: uhd_fft -A TX/RX -s 50M -f 2310M

CBRS:
- Overall 3550-3700
- GAA: 3550-3700
- PAL: 3550-3650
- Grandfathered/gaa: 3650-3700

POWDER is using:
- CBRS: 3550-3600
- Low C-band: 3358 - 3550

Scan range:
Activity:
- Maybe gap at: 3350-3470 (-100dB)
- 3470-3510
- Maybe gap at: 3510-3550 (-100dB)

CBRS:
- 3550-3570 (PAL)
- 3570-3590 (PAL)
- 3590-3610 (PAL)
- 3610-3630 (PAL)
- 3630-3650 (PAL)
- 3650-3670 (GAA)
- 3670-3690 (GAA)
- maybe a gap at 3690-3700 (-99dB)

Also at: (looks like Verizon/AT&T licenses)
- 3700-3760
- 3760-3800
-->

#### Optional exploration

##### Repeat measurements using a different radio/experiment

Try to repeat some of the spectrum monitoring activities while using a different radio/experiment. (Note: If you try to access the VNC window for another node and it fails to open it means that someone else still has an ongoing VNC session on that node.)

##### Parameterized profiles

"Hard coding" resources into a profile like we have done above is not how you would typically create a profile you want to use long term. Adding selectable parameters to your profile is a much better way of doing things.

Here is an example profile, similar to the one we created, that uses parameters: [https://www.powderwireless.net/p/PowderProfiles/gnuradio_ota](https://www.powderwireless.net/p/PowderProfiles/gnuradio_ota)

Try to copy and extend this profile (or the one we used earlier) to create a single parameterized version of the profile we used in the earlier part of the hands-on session.


